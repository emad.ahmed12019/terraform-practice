provider "aws" {
  region = "us-west-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
resource "aws_vpc" "dev-env" {
  cidr_block = var.aws_vpc_dev_env
  tags = {
    Name = var.environment
  }

}
variable "subnet_cidr_block" {
  description = "cidr block subnet"
  default = "10.0.0.0/24"
  type = 
}
variable "stag_subnet_cidr_block" {
  description = "cidr block subnet staging"
}

variable "aws_vpc_dev_env" {
  description = " aws vpc dev "
}
variable "aws_secret_key" {
  description = " aws secret key  "
}

variable "aws_access_key" {
  description = " aws access key  "
}


variable "environment" {
  description = " aws environment  "
}

resource "aws_subnet" "dev-env-subnet1" {
  vpc_id     = aws_vpc.dev-env.id
  cidr_block = var.subnet_cidr_block
  availability_zone = "us-west-1a"
  tags = {
    Name = "dev-env-subnet1"
  }
}


data "aws_vpc" "exisiting_vpc" {
  default = true
}
resource "aws_subnet" "stag-env-subnet1" {
  vpc_id     = data.aws_vpc.exisiting_vpc.id
  cidr_block = var.stag_subnet_cidr_block
  availability_zone = "us-west-1b"
  tags = {
    Name = "stag-env-subnet1"
  }
}

output "stag-subnet-id" {
  value = aws_subnet.stag-env-subnet1.id

}
output "dev-subnet-id" {
  value = aws_subnet.dev-env-subnet1.id
}

 
